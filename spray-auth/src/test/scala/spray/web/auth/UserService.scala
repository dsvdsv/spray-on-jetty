package spray.web.auth

import scala.concurrent.Future


/**
 * Created by dsvdsv on 05.09.15.
 */
case class Account(username: String, password: String)

object Users {
	val humans = Map(
		"john" -> Account("john", "smith"),
		"joe" -> Account("joe", "average"),
		"john_expired" -> Account("john_expired", "smith"),
		"john_locked" -> Account("john_locked", "smith"),
		"john_disabled" -> Account("john_disabled", "smith")
	)

	val userService = new UserService[Account] {
		override def authenticate(login: String, password: String): Future[Option[UserAccount[Account]]] =
			Future.successful(humans.get(login).find(_.password == password).map(UserAccount(login, _)))

		override def postAuthenticate(account: UserAccount[Account]): Either[PostAuthRejection[Account], UserAccount[Account]] = {
			account.user.username match {
				case "john_expired" => Left(AccountExpiredRejection(account))
				case "john_locked" => Left(AccountLockedRejection(account))
				case "john_disabled" => Left(AccountDisabledRejection(account))
				case _ => Right(account)
			}
		}
	}
}
