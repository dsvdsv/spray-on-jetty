package spray.web.auth

import akka.actor.ActorRefFactory
import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner
import org.scalatest.{FunSuite, Matchers}
import spray.http.HttpHeaders.{Cookie, Location, RawHeader, `Set-Cookie`}
import spray.http.{FormData, HttpCookie, StatusCodes, Uri}
import spray.routing._
import spray.testkit.ScalatestRouteTest
import spray.web.session.{InMemorySessionManager, SessionSettings}

import scala.concurrent.duration._

/**
 * Created by dsvdsv on 05.09.15.
 */
@RunWith(classOf[JUnitRunner])
class AuthSpec extends FunSuite with Matchers with ScalatestRouteTest with HttpService with LoginDirectives {
	implicit val sessionManager = new InMemorySessionManager
	val userService = Users.userService
	val loginSetting = implicitly[LoginSettings]
	val sessionSettings = implicitly[SessionSettings]

	val loginService = LoginService(userService)
	val authRoute =
		path("ping") {
			login(loginService) { userInfo =>
				get {
					complete(s"authenticated ${userInfo.id}")
				}
			}
		} ~ loginService.route

	def actorRefFactory: ActorRefFactory = system

	def remoteHostName = "example.com"

	def extractSessionId: String = {
		header[`Set-Cookie`].get.cookie.content
	}


	test("Any request should redirect to login") {
		Get("/ping") ~> authRoute ~> check {
			assert(header[Location] === Some(Location(Uri.Empty.withPath(loginSetting.loginPath).withQuery("return_page=ping"))))
			assert(status === StatusCodes.Found)
		}
	}

	test(s"Brocken auth request should redirect to ${loginService.loginPath.toString()}") {
		Post("/login", FormData(Seq("username" -> "smith", "password" -> "smith"))) ~> authRoute ~> check {
			assert(header[Location] === Some(Location(Uri.Empty.withPath(loginSetting.loginPath).withQuery("error=failed"))))
			assert(status === StatusCodes.Found)
		}
	}

	test("Correct auth request with referer should success auth") {
		val url = "http://localhost:8080/domain"
		val referer = url + "/login"
		Post("/login", FormData(Seq("login" -> "john", "password" -> "smith"))) ~> RawHeader("Referer", referer) ~> authRoute ~> check {
			assert(header[Location] === Some(Location(Uri(url))))
			assert(status === StatusCodes.Found)
			val sessionId = extractSessionId
			Get("/ping") ~> Cookie(HttpCookie(sessionSettings.cookieName, sessionId)) ~> authRoute ~> check {
				assert(responseAs[String] == "authenticated john")
			}
		}
	}

	test("Ping request with referer should redirect to login, after logout") {
		val url = "http://localhost:8080/domain"
		val referer = url + "/login"
		Post("/login", FormData(Seq("login" -> "john", "password" -> "smith"))) ~> RawHeader("Referer", referer) ~> authRoute ~> check {
			assert(header[Location] === Some(Location(Uri(url))))
			assert(status === StatusCodes.Found)
			val sessionId = extractSessionId
			Get("/logout") ~> Cookie(HttpCookie(sessionSettings.cookieName, sessionId)) ~> authRoute ~> check {
				assert(header[Location] === Some(Location(Uri.Empty.withPath(loginSetting.loginPath))))
				assert(status === StatusCodes.Found)
				Get("/ping") ~> Cookie(HttpCookie(sessionSettings.cookieName, sessionId)) ~> authRoute ~> check {
					assert(header[Location] === Some(Location(Uri.Empty.withPath(loginSetting.loginPath).withQuery("return_page=ping"))))
					assert(status === StatusCodes.Found)
				}
			}
		}
	}

	test("Requests with referer should redirect to login, after session expire") {
		val url = "http://localhost:8080/domain"
		val referer = url + "/login"
		Post("/login", FormData(Seq("login" -> "john", "password" -> "smith"))) ~> RawHeader("Referer", referer) ~> authRoute ~> check {
			assert(header[Location] === Some(Location(Uri(url))))
			assert(status === StatusCodes.Found)
			val sessionId = extractSessionId
			val session = sessionManager.findSessionById(sessionId).get
			session.maxInactive = 1.minute
			Thread.sleep((session.maxInactive + 10.seconds).toMillis)
			Get("/ping") ~> Cookie(HttpCookie(sessionSettings.cookieName, sessionId)) ~> authRoute ~> check {
				assert(header[Location] === Some(Location(Uri.Empty.withPath(loginSetting.loginPath).withQuery("return_page=ping"))))
				assert(status === StatusCodes.Found)
			}
		}
	}

	test("Correct auth request should success auth") {
		Post("/login", FormData(Seq("login" -> "john", "password" -> "smith"))) ~> authRoute ~> check {
			assert(header[Location] === Some(Location(Uri("http://localhost:8080"))))
			assert(status === StatusCodes.Found)
			val sessionId = extractSessionId
			Get("/ping") ~> Cookie(HttpCookie(sessionSettings.cookieName, sessionId)) ~> authRoute ~> check {
				assert(responseAs[String] == "authenticated john")
			}
		}
	}

	test("Ping request should redirect to login, after logout") {
		Post("/login", FormData(Seq("login" -> "john", "password" -> "smith"))) ~> authRoute ~> check {
			assert(header[Location] === Some(Location(Uri("http://localhost:8080"))))
			assert(status === StatusCodes.Found)
			val sessionId = extractSessionId
			Get("/logout") ~> Cookie(HttpCookie(sessionSettings.cookieName, sessionId)) ~> authRoute ~> check {
				assert(header[Location] === Some(Location(Uri.Empty.withPath(loginSetting.loginPath))))
				assert(status === StatusCodes.Found)
				Get("/ping") ~> Cookie(HttpCookie(sessionSettings.cookieName, sessionId)) ~> authRoute ~> check {
					assert(header[Location] === Some(Location(Uri.Empty.withPath(loginSetting.loginPath).withQuery("return_page=ping"))))
					assert(status === StatusCodes.Found)
				}
			}
		}
	}

	test("Requests should redirect to login, after session expire") {
		Post("/login", FormData(Seq("login" -> "john", "password" -> "smith"))) ~> authRoute ~> check {
			assert(header[Location] === Some(Location(Uri("http://localhost:8080"))))
			assert(status === StatusCodes.Found)
			val sessionId = extractSessionId
			val session = sessionManager.findSessionById(sessionId).get
			session.maxInactive = 1.minute
			Thread.sleep((session.maxInactive + 10.seconds).toMillis)
			Get("/ping") ~> Cookie(HttpCookie(sessionSettings.cookieName, sessionId)) ~> authRoute ~> check {
				assert(header[Location] === Some(Location(Uri.Empty.withPath(loginSetting.loginPath).withQuery("return_page=ping"))))
				assert(status === StatusCodes.Found)
			}
		}
	}

	test("Incorrect auth request should redirect to login") {
		Post("/login", FormData(Seq("login" -> "john1", "password" -> "smith"))) ~> authRoute ~> check {
			assert(header[Location] === Some(Location(Uri().withPath(loginSetting.loginPath).withQuery("error=failed"))))
			assert(status === StatusCodes.Found)
		}
	}

	test("Expired account should redirect to login with expired error") {
		Post("/login", FormData(Seq("login" -> "john_expired", "password" -> "smith"))) ~> authRoute ~> check {
			assert(header[Location] === Some(Location(Uri().withPath(loginSetting.loginPath).withQuery("error=expired"))))
			assert(status === StatusCodes.Found)
		}
	}

	test("Locked account should redirect to login with locked error") {
		Post("/login", FormData(Seq("login" -> "john_locked", "password" -> "smith"))) ~> authRoute ~> check {
			assert(header[Location] === Some(Location(Uri().withPath(loginSetting.loginPath).withQuery("error=locked"))))
			assert(status === StatusCodes.Found)
		}
	}

	test("Disabled account should redirect to login with disabled error") {
		Post("/login", FormData(Seq("login" -> "john_disabled", "password" -> "smith"))) ~> authRoute ~> check {
			assert(header[Location] === Some(Location(Uri().withPath(loginSetting.loginPath).withQuery("error=disabled"))))
			assert(status === StatusCodes.Found)
		}
	}
}
