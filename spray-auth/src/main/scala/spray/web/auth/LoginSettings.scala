package spray.web.auth

import akka.actor.ActorRefFactory
import com.typesafe.config.Config
import spray.http.Uri
import spray.http.Uri.Path
import spray.util._

case class LoginSettings(loginPath: Path, defaultReturnUri: Uri)

object LoginSettings extends SettingsCompanion[LoginSettings]("spray.login") {

	override def fromSubConfig(c: Config): LoginSettings =
		LoginSettings(
			getRelativeUriPath(c.getString("login-path")),
			Uri(c.getString("default-return-uri"))
		)

	def getRelativeUriPath(path: String): Path = {
		val p = Path(path)
		require(!p.startsWithSlash)
		p
	}

	implicit def default(implicit refFactory: ActorRefFactory) = apply(actorSystem)
}
