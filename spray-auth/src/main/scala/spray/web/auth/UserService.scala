package spray.web.auth

import spray.routing.Rejection

import scala.concurrent.Future

/**
 * User: Dikansky
 * Date: 31.08.2015
 */
trait UserService[U] {
	def authenticate(login: String, password: String): Future[Option[UserAccount[U]]]

	def postAuthenticate(userAccount: UserAccount[U]): Either[PostAuthRejection[U], UserAccount[U]]
}

case class UserAccount[U](id: String, user: U)


sealed trait PostAuthRejection[U] extends Rejection {
	val account: UserAccount[U]
}

case class AccountLockedRejection[U](account: UserAccount[U]) extends PostAuthRejection[U]
case class AccountDisabledRejection[U](account: UserAccount[U]) extends PostAuthRejection[U]
case class AccountExpiredRejection[U](account: UserAccount[U]) extends PostAuthRejection[U]
