package spray.web

import shapeless.HNil
import spray.http.Uri
import spray.http.Uri.{Path, Query}
import spray.routing._
import spray.routing.authentication.ContextAuthenticator

/**
 * User: Dikansky
 * Date: 31.08.2015
 */
package object auth {
	type LoginAuthenticator[U] = ContextAuthenticator[UserAccount[U]]
	type SuccessHandler = Uri => Route
	val PrincipalKey = "principal"

	def asMatcher(path: Path): PathMatcher0 = PathMatcher(path, HNil)

	def putIf(query: Query, kv: (String, Option[String])): Query = if (kv._2.isDefined) query.+:(kv._1 -> kv._2.get) else query
}
