package spray.web.auth

import akka.actor.ActorRefFactory
import spray.http.StatusCodes.Found
import spray.http.Uri.{Path, Query}
import spray.http._
import spray.routing.AuthenticationFailedRejection.CredentialsMissing
import spray.routing._
import spray.routing.directives.AuthMagnet
import spray.web.session._

import scala.concurrent.{ExecutionContext, Future}
import scala.language.{implicitConversions, postfixOps}


/**
  * User: Dikansky
  * Date: 31.08.2015
  */
trait LoginService[U] {
  def userService: UserService[U]

  def loginPath: Path

  def withLoginPath(path: Path): LoginService[U]

  def defaultReturnUri: Uri

  def withDefaultReturnUri(uri: Uri): LoginService[U]

  def route: Route

  def authenticator: LoginAuthenticator[U]

  def redirectToLoginPage(returnUri: Option[Uri]): Route
}

object LoginService {
  def apply[U](userService: UserService[U], rejectionHandler: Option[RejectionHandler] = None, successHandler: Option[SuccessHandler] = None)(implicit actorRefFactory: ActorRefFactory,
                                                                                                                                              settings: LoginSettings,
                                                                                                                                              sm: SessionManager,
                                                                                                                                              ec: ExecutionContext): LoginService[U] =
    new LoginServiceImpl[U](userService, settings.loginPath, rejectionHandler, successHandler, settings.defaultReturnUri)

  implicit def toAuthMagnet[U](service: LoginService[U])(implicit ec: ExecutionContext): AuthMagnet[UserAccount[U]] =
    AuthMagnet.fromContextAuthenticator(service.authenticator)
}


private case class LoginServiceImpl[U](val userService: UserService[U], val loginPath: Path, rejectionHandler: Option[RejectionHandler], successHandler: Option[SuccessHandler],
                                       val defaultReturnUri: Uri)(implicit actorRefFactory: ActorRefFactory, sm: SessionManager, ec: ExecutionContext)
  extends LoginService[U] with Directives with SessionDirectives {

  override val authenticator: LoginAuthenticator[U] = { ctx ⇒
    def missing: Future[Left[AuthenticationFailedRejection, Nothing]] = {
      Future.successful(Left(AuthenticationFailedRejection(AuthenticationFailedRejection.CredentialsMissing, List())))
    }
    sm.findSessionByRequest(ctx.request) match {
      case Some(session) => session.attribute[UserAccount[U]](PrincipalKey) match {
        case Some(userInfo) => Future.successful(Right(userInfo))
        case None => missing
      }
      case None => missing
    }
  }

  val defaultRejectionHandler = RejectionHandler {
    case AccountLockedRejection(_) :: _ => redirect(Uri.Empty.withPath(loginPath).withQuery("error" -> "locked"), Found)
    case AccountExpiredRejection(_) :: _ => redirect(Uri.Empty.withPath(loginPath).withQuery("error" -> "expired"), Found)
    case AccountDisabledRejection(_) :: _ => redirect(Uri.Empty.withPath(loginPath).withQuery("error" -> "disabled"), Found)
    case _ => redirect(Uri().withPath(loginPath).withQuery("error" -> "failed"), Found)
  }

  val defaultSuccessHandler: SuccessHandler = (uri: Uri) => redirect(uri, Found)

  val sh = successHandler.getOrElse(defaultSuccessHandler)
  val rh = rejectionHandler.getOrElse(defaultRejectionHandler)

  private def getReturnPage(referer: Option[String], returnPage: Uri): Uri = {
    if (referer.isEmpty) {
      returnPage
    } else {
      val text = referer.get
      Uri(
        text.slice(0, text.indexOf("/" + loginPath.toString()))
      )
    }
  }

  val route =
    path(asMatcher(loginPath)) {
      post {
        parameter("return_page".as[Uri] ? defaultReturnUri) { returnPage =>
          session() { session =>
            optionalHeaderValueByName("Referer") { referer =>
              handleRejections(rh) {
                formFields("login", "password") { (login, password) ⇒
                  onSuccess(userService.authenticate(login, password)) {
                    case Some(userInfo) => {
                      userService.postAuthenticate(userInfo) match {
                        case Right(userAccount) => {
                          session.attribute(PrincipalKey, userInfo)
                          sh(getReturnPage(referer,returnPage))
                        }
                        case Left(rejection) => reject(rejection)
                      }
                    }
                    case None => reject(AuthenticationFailedRejection(CredentialsMissing, List()))
                  }
                }
              }
            }
          }
        }
      }
    } ~
      path("logout") {
        (get | put) {
          session() { session =>
            session.invalidate
            redirectToLoginPage(None)
          }
        }
      }

  override def redirectToLoginPage(returnUri: Option[Uri]): Route = {
    val query = putIf(Query(), ("return_page" -> returnUri.map(_.path.tail.toString())))
    val url = Uri.Empty.withPath(loginPath).withQuery(query)
    redirect(url, Found)
  }

  override def withLoginPath(path: Path): LoginService[U] = copy(loginPath = path)

  override def withDefaultReturnUri(uri: Uri): LoginService[U] = copy(defaultReturnUri = uri)
}
