package spray.web.auth

import spray.routing.Directives._
import spray.routing.{Directive1, Route}

import scala.concurrent.ExecutionContext
import scala.language.implicitConversions

trait LoginDirectives {
	def login[U](magnet: LoginMagnet[U]): Directive1[UserAccount[U]] = magnet.directive
}

object LoginDirectives extends LoginDirectives

class LoginMagnet[U](loginService: LoginService[U])(implicit ec: ExecutionContext) {
	val directive = authenticate(loginService) | extract(_.request.uri).flatMap { uri =>
		Route.toDirective(loginService.redirectToLoginPage(Some(uri))): Directive1[UserAccount[U]]
	}
}

object LoginMagnet {
	implicit def fromLoginService[U](service: LoginService[U])(implicit ec: ExecutionContext): LoginMagnet[U] =
		new LoginMagnet(service)
}
