package spray.web

import akka.actor.{ActorSystem, Props}
import spray.servlet.WebBoot

/**
 * Created by dsvdsv on 24.08.15.
 */
class Boot extends WebBoot {
	val system = ActorSystem("spray-on-jetty")

	val serviceActor = system.actorOf(Props[DemoServiceActor])

	system.registerOnTermination {
		system.log.info("Application shut down")
	}
}
