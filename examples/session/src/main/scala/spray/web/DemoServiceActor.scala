package spray.web

import akka.actor.Actor
import spray.routing.HttpService
import spray.routing.authentication.UserPass
import spray.web.session._

import scala.concurrent.Future
import scala.language.postfixOps

/**
 * Created by dsvdsv on 24.08.15.
 */
class DemoServiceActor extends Actor with DemoService {
	def actorRefFactory = context

	override def receive = runRoute(demoRoute)
}

trait DemoService extends HttpService with SessionDirectives {
	implicit val sessionManager = new InMemorySessionManager

	val demoRoute = {
		get {
			pathSingleSlash {
				complete("Hello world")
			} ~
				path("get") {
					session() { session ⇒
						complete {
							val value = session.attribute[Int]("attr2")
							s"attr2=$value"
						}
					}
				} ~
				path("set") {
					session() { session =>
						complete {
							session.attribute("attr2", 100)
							"attr2 setted"
						}
					}
				} ~
				path("recreate") {
					session() { s1 =>
						session() { s2 =>
							complete {
								s"${s1.id == s2.id}"
							}
						}
					}
				}
		}
	}
	var i = 0

	// we use the enclosing ActorContext's or ActorSystem's dispatcher for our Futures and Scheduler
	implicit def executionContext = actorRefFactory.dispatcher

	def myUserPassAuthenticator(userPass: Option[UserPass]): Future[Option[String]] =
		Future {
			if (userPass.exists(up => up.user == "John" && up.pass == "p4ssw0rd")) Some("John")
			else None
		}

	def ping: String = {
		i += 1
		"Pong"
	}

}