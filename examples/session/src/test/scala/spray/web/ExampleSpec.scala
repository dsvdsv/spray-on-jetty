package spray.web

import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner
import org.scalatest.{FunSuite, Matchers}
import spray.routing.HttpService
import spray.testkit.ScalatestRouteTest

@RunWith(classOf[JUnitRunner])
class ExampleSpec extends FunSuite with Matchers with ScalatestRouteTest with HttpService with DemoService {
	def actorRefFactory = system

	test("The service retrun 'Hello world' for single slash path") {
		Get() ~> demoRoute ~> check {
			val res = responseAs[String]

			assert(res == "Hello world")
		}
	}
}
