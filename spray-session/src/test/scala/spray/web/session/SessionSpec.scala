package spray.web.session

import akka.actor.ActorRefFactory
import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner
import org.scalatest.{FunSuite, Matchers}
import spray.http.HttpHeaders.{Cookie, `Set-Cookie`}
import spray.http.{HttpCookie, StatusCodes}
import spray.routing.HttpService
import spray.testkit.ScalatestRouteTest

import scala.concurrent.duration._

/**
 * User: Dikansky
 * Date: 03.09.2015
 */
@RunWith(classOf[JUnitRunner])
class SessionSpec extends FunSuite with Matchers with ScalatestRouteTest with HttpService with SessionDirectives {
	def actorRefFactory: ActorRefFactory = system

	implicit val sessionManager = new InMemorySessionManager
	val settings = implicitly[SessionSettings]

	val sessionRoute = {
		get {
			pathSingleSlash {
				complete("Hello world")
			} ~
				path("get") {
					session() { session =>
						complete {
							val value = session.attribute[Int]("attr2")
							s"attr2=$value"
						}
					}
				} ~
				path("set") {
					session() { session =>
						complete {
							session.attribute("attr2", 100)
							"attr2 setted"
						}
					}
				} ~
				path("recreate") {
					session() { s1 =>
						session() { s2 =>
							complete {
								s"${s1.id == s2.id}"
							}
						}
					}
				}
		}
	}

	def extractSessionId: String = {
		header[`Set-Cookie`].get.cookie.content
	}

	test("Response should have sessionid") {
		Get("/set") ~> sessionRoute ~> check {
			val setCookie = header[`Set-Cookie`]
			val res = responseAs[String]
			val session = sessionManager.sessions.stream().findFirst().get()
			assert(res == "attr2 setted")
			assert(header[`Set-Cookie`] === Some(`Set-Cookie`(HttpCookie(settings.cookieName, content = session.id, httpOnly = settings.httpOnly))))
		}
	}

	test("Session should have setted attribute") {
		Get("/set") ~> sessionRoute ~> check {
			assert(status === StatusCodes.OK)
			val sessionId = extractSessionId
			Get("/get") ~> Cookie(HttpCookie(settings.cookieName, sessionId)) ~> sessionRoute ~> check {
				val res = responseAs[String]
				val session = sessionManager.findSessionById(sessionId)
				val value = session.get.attribute("attr2")
				assert(value == Some(100))
				assert(res == "attr2=Some(100)")
			}
		}
	}


	test("Session should expire after timeout") {
		Get("/set") ~> sessionRoute ~> check {
			assert(status === StatusCodes.OK)
			val sessionId = extractSessionId
			val session = sessionManager.findSessionById(sessionId).get
			session.maxInactive = 1.minute
			Thread.sleep((session.maxInactive + 10.seconds).toMillis)
			Get("/get") ~> Cookie(HttpCookie(settings.cookieName, sessionId)) ~> sessionRoute ~> check {
				val res = responseAs[String]
				assert(sessionManager.findSessionById(sessionId) == None)
				assert(res == "attr2=None")
			}
		}
	}

	test("Session should have manual invalidation") {
		Get("/set") ~> sessionRoute ~> check {
			assert(status === StatusCodes.OK)
			val sessionId = extractSessionId
			val session = sessionManager.findSessionById(sessionId).get
			session.invalidate()
			Get("/get") ~> Cookie(HttpCookie(settings.cookieName, sessionId)) ~> sessionRoute ~> check {
				val res = responseAs[String]
				assert(sessionManager.findSessionById(sessionId) == None)
				assert(res == "attr2=None")
			}
		}
	}

	//	test("Session should not be re creation") {
	//		Get("/recreate") ~> demoRoute ~> check {
	//			val res = responseAs[String]
	//			assert(res == "true")
	//		}
	//	}
}
