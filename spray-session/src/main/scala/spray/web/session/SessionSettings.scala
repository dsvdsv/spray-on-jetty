package spray.web.session

import akka.actor.ActorRefFactory
import com.typesafe.config.Config
import spray.util._

import scala.concurrent.duration._

/**
 * Created by dsvdsv on 26.08.15.
 */

case class SessionSettings(
														maxSessionInactive: Duration,
														cookieName: String,
														maxAge: Option[Long],
														domain: Option[String],
														path: Option[String],
														secure: Boolean,
														httpOnly: Boolean)

object SessionSettings extends SettingsCompanion[SessionSettings]("spray.session") {

	override def fromSubConfig(c: Config): SessionSettings =
		SessionSettings(
			Duration(c.getString("timeout")),
			c.getString("cookieName"),
			None,
			None,
			None,
			c.getBoolean("secure"),
			c.getBoolean("httpOnly"))


	implicit def default(implicit refFactory: ActorRefFactory): SessionSettings = apply(actorSystem)
}
