package spray.web.session

import java.time.LocalTime
import java.util.UUID
import java.util.concurrent.CopyOnWriteArraySet
import java.util.function.Consumer

import com.typesafe.scalalogging.LazyLogging

/**
 * Created by dsvdsv on 28.08.15.
 */
class InMemorySessionManager(implicit settings: SessionSettings) extends SessionManager with LazyLogging {
	private[session] val sessions = new CopyOnWriteArraySet[Session]

	override def addNew: Session = {
		val id = UUID.randomUUID().toString
		val session = new InMemorySession(id, this)
		logger.debug(s"Create new session ${id}")
		add(session)
		session
	}

	override def remove(session: Session): Unit = {
		if (sessions.remove(session))
			logger.debug(s"Remove session ${session.id}")
	}

	override def add(session: Session): Unit = {
		if (sessions.add(session))
			logger.debug(s"Added session ${session.id}")
	}

	override def findSessionById(id: String): Option[Session] = {
		val opts = sessions.stream().filter(_.id == id).findFirst()
		if (opts.isPresent) {
			val session = opts.get()
			session.touch()
			Some(session)
		} else None
	}

	override private[session] def scavenge(): Unit = {
		sessions.stream().forEach(new Consumer[Session] {
			override def accept(session: Session): Unit = {
				val id = session.id
				val lastAccessedTime = session.lastAccessedTime
				if (session.checkExpiry(LocalTime.now())) {
					logger.debug(s"Session $id is expired, lastAccessTime = $lastAccessedTime")
					session.invalidate()
				}
			}
		})
	}
}
