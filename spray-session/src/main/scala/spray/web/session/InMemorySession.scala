package spray.web.session

import com.typesafe.scalalogging.LazyLogging

import scala.collection.mutable

/**
 * Created by dsvdsv on 26.08.15.
 */
class InMemorySession(val id: String, val manager: SessionManager)(implicit settings: SessionSettings) extends Session with LazyLogging {
	private val attributes = new mutable.HashMap[String, Any]

	var maxInactive = settings.maxSessionInactive

	override def attribute[T](name: String): Option[T] = {
		touch()
		logger.debug(s"Read attribute ${name} in session ${id}")
		attributes.get(name) match {
			case Some(v) => Some(v.asInstanceOf[T])
			case _ => None
		}
	}

	override def attribute[T](name: String, value: T): Session = {
		touch()
		logger.debug(s"Write attribute ${name} in session ${id}")
		attributes(name) = value
		this
	}

	override def remove(key: String): Session = {
		touch()
		attributes.remove(key)
		this
	}

	override def invalidate(): Unit = {
		super.invalidate()
		attributes.clear()
	}
}
