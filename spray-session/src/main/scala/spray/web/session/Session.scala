package spray.web.session

import java.time.LocalTime

import com.typesafe.scalalogging.LazyLogging

import scala.concurrent.duration.Duration

/**
 * Created by dsvdsv on 26.08.15.
 */
trait Session extends LazyLogging {

	val creationTime = LocalTime.now()

	var lastAccessedTime = creationTime

	var maxInactive: Duration

	def isNew = creationTime == lastAccessedTime

	def id: String

	def attribute[T](name: String): Option[T]

	def attribute[T](name: String, value: T): Session

	def remove(name: String): Session

	private[session] def manager: SessionManager

	private[session] def touch(time: LocalTime = LocalTime.now()): Boolean = synchronized {
		if (checkExpiry(time)) {
			logger.debug(s"Session $id is expired, lastAccessTime = $lastAccessedTime")
			invalidate()
			false
		} else {
			lastAccessedTime = time
			true
		}
	}

	def invalidate(): Unit = {
		manager.remove(this)
	}

	/**
	 * Check to see if session has expired as at the time given.
	 * @param time time to check, default equals current time
	 * @return true if session expiry
	 */
	private[session] def checkExpiry(time: LocalTime = LocalTime.now()):Boolean = {
		val duration = Duration.fromNanos(java.time.Duration.between(lastAccessedTime, time).toNanos)
		duration > maxInactive
	}
}
