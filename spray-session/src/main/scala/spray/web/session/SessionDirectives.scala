package spray.web.session

import spray.http.HttpCookie
import spray.routing.Directives._
import spray.routing._

import scala.language.implicitConversions


trait SessionDirectives {
	def session(magnet: SessionMagnet): Directive1[Session] = magnet()
}

trait SessionMagnet {
	def apply(): Directive1[Session]
}

object SessionMagnet {
	implicit def fromUnit(u: Unit)(implicit sm: SessionManager, settings: SessionSettings): SessionMagnet = new SessionMagnet {
		override def apply(): Directive1[Session] = {
			def createSession: Directive1[Session] = {
				val newSession = sm.addNew
				setupCookie(newSession, settings) & provide(newSession)
			}

			optionalCookie(settings.cookieName).flatMap {
				case Some(cookie) => sm.findSessionById(cookie.content) match {
					case Some(session) => provide(session)
					case None => createSession
				}
				case None ⇒ createSession
			}
		}
	}


	private def setupCookie(session: Session, settings: SessionSettings): Directive0 = {
		val cookie = HttpCookie(settings.cookieName,
			session.id, maxAge = settings.maxAge,
			domain = settings.domain, path = settings.path,
			secure = settings.secure, httpOnly = settings.httpOnly)

		setCookie(cookie)
	}
}