package spray.web.session

import java.util.concurrent.{CopyOnWriteArrayList, Executors}
import java.util.function.Consumer

import spray.http.HttpRequest

import scala.concurrent.duration._

/**
 * Created by dsvdsv on 26.08.15.
 */
trait SessionManager {

	SessionManager.accept(this)

	def remove(session: Session): Unit

	def add(session: Session): Unit

	def findSessionById(id: String): Option[Session]

	def findSessionByRequest(request: HttpRequest)(implicit settings: SessionSettings): Option[Session] = {
		request.cookies.find(_.name == settings.cookieName) match {
			case Some(cookie) => findSessionById(cookie.content)
			case _ => None
		}
	}

	def addNew: Session

	private[session] def scavenge(): Unit
}

object SessionManager {
	val managers = new CopyOnWriteArrayList[SessionManager]()
	val ScavengePeriod = 5.seconds
	val scheduler = Executors.newScheduledThreadPool(1)

	scheduler.scheduleAtFixedRate(() => {
		scan()
	}, ScavengePeriod.toSeconds, ScavengePeriod.toSeconds, SECONDS)

	def accept(manager: SessionManager) = managers.add(manager)

	def scan(): Unit = {
		managers.stream().forEach(
			new Consumer[SessionManager] {
				override def accept(t: SessionManager): Unit = t.scavenge()
			}
		)
	}

}